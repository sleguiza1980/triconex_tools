from configparser import ConfigParser
import sys
import logging
import progressbar
from datetime import datetime
from requests import post, get
from anviz import Device
import json

def sync_attendace(index=0, force_all=False):
    logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO, filename = 'log/'+datetime.now().strftime('%Y_%m_%d.log'))
    config = ConfigParser()
    config.read('config.ini')

    # config device
    dev_id = config.getint(f'device{index}', 'device_id')
    ip_addr = config.get(f'device{index}', 'ip_addr')
    ip_port = config.getint(f'device{index}', 'ip_port')
    clock = Device(dev_id, ip_addr, ip_port)

    # config api
    api_uri = config.get('api', 'uri_attendance')
    customer = config.get('api', 'customer')
    # Check stored db
    count = 1
    if count == 0 or force_all:
        only_new = False
    else:
        only_new = True

    rtest = get(url = api_uri)
    if rtest.status_code != 200:
        logging.warning('Failed to connect with API')
        raise RuntimeError('Failed to connect with API')
    try:
        total = getattr(clock.get_record_info(), 'new_records')
    except:
        logging.warning(f'sync attendance - Failed to connect (device: {dev_id} - ip: {ip_addr})')
        print(f'sync attendance - Failed to connect (device: {dev_id} - ip: {ip_addr})')
        return
        
    bar = progressbar.ProgressBar(maxval=total, \
        widgets=[f'sync attendance (device: {dev_id} - ip: {ip_addr})',progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()
    i = 0
    try:
        for index,record in enumerate(clock.download_records(True)):
            i += 1
            attendance_record = {
                    'device_id': dev_id,
                    'user_code':record.code,
                    'id_customer': customer,
                    'datetime':record.datetime,
                    'type_code':"IN" if record.type == 0 else "OUT"
            }
            r = post(url = api_uri, data = attendance_record)
            if r.status_code != 200:
                # store
                logging.warning('Failed to connect with API')
                raise RuntimeError('Failed to connect with API')
            logging.info(f'device_id:{dev_id}, datetime: {record.datetime}, user_code: {record.code}, type_code: {record.type}')  
            bar.update(i)
    except:
        logging.warning(f'sync attendance - Failed to connect (device: {dev_id} - ip: {ip_addr})')
        print(f'sync attendance - Failed to connect (device: {dev_id} - ip: {ip_addr})')
    bar.finish()

def sync_staff(index = 0, dicts = []):
    logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO, filename = 'log/'+datetime.now().strftime('%Y_%m_%d.log'))
    config = ConfigParser()
    config.read('config.ini')
    
    # config device
    dev_id = config.getint(f'device{index}', 'device_id')
    ip_addr = config.get(f'device{index}', 'ip_addr')
    ip_port = config.getint(f'device{index}', 'ip_port')
    clock = Device(dev_id, ip_addr, ip_port)
    
    api_uri = config.get('api', 'uri_staff')
    customer = config.get('api', 'customer')
    #r = get(url = f'{api_uri}?id_customer={customer}')
    r = get(url = f'{api_uri}?id_customer={customer}&id_device={dev_id}')
    if r.status_code != 200:
        logging.warning('Failed to connect with API')
        raise RuntimeError('Failed to connect with API') 
    #dicts = []
    #for i in range(30):
    #    dicts = dicts +  [{ "name": "EV"+str(i), "dni": 202000+i }]
    dicts = json.loads(r.text)
    dicts = dicts["rs"]
    try:
        clock.upload_staff_info(dicts)
        q = min([12, len(dicts)])
        left = len(dicts) - q
        bar = progressbar.ProgressBar(maxval=len(dicts), \
            widgets=[f'sync staff (device: {dev_id} - ip: {ip_addr})',progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
        bar.start()
        del dicts[:q]
        i = q
        bar.update(i)
        while left > 0:
            q = min([12, len(dicts)])
            i = i + q
            left = len(dicts) - q
            clock.upload_staff_info(dicts)
            bar.update(i)
            del dicts[:q]
    except:
        logging.warning(f'sync staff - Failed to connect (device: {dev_id} - ip: {ip_addr})')
        print(f'sync staff - Failed to connect (device: {dev_id} - ip: {ip_addr})')
        return
        
def create_staff():
    records = [{ "name": "KLLKLK", "dni": 2020004, "card":"dd4211" }]
    clock = Device(6, "192.168.6.55", 5010)
    clock.upload_staff_info(dicts)

def create_records():
    records = [{ "dni": 2020004, "datetime": 691649400, "type_code": 1}]
    clock = Device(5, "192.168.6.54", 5010)
    valor = clock.upload_records(dicts)
    print(valor)
    
def download_staff():
    clock = Device(5, '192.168.6.54', 5010)
    valor  = clock.download_staff_info()
    print (valor)    
    
def main():
    force_all = '--all' in sys.argv
    config = ConfigParser()
    config.read('config.ini')
    devices = config.getint('anviz', 'devices')
    
    for i in range(devices):
        sync_attendace(index = i, force_all=force_all)

    for i in range(devices):
        sync_staff(index = i)        

if __name__ == '__main__':
    #create_staff()
    #create_records()
    #download_staff()
    try:
        main()
    except Exception as e:
        logging.warning(str(e))
        sys.exit(str(e))
