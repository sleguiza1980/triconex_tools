## Protocolo ##

El protocolo utilizado por los dispositivos anviz es el que se encuentra en el documento comms-protocol.pdf [Link](https://github.com/WhiteaglePT/node-anviz/blob/master/dev/tcb_communications_protocol2.5.pdf)

---
## Instalación de dependencias ##
pip install -r requirements.txt

---
## Generar ejecutable para windows (.exe) ##
pyinstaller sender.py --onefile

---
## Instalación ##
Para el correcto funcionamiento se debe:

- Implementar la api nodejs en la red servidor (TC), ejecutar el archivo server.js. Se realiza por única vez, para la interacción con la base de datos de controlite.

- Por cada instalación en el cliente se debe generar un directorio que debe contener los archivos sender.exe, config.ini y una carpeta llamada log.

- Crear una tarea en el programador de tareas de la máquina donde va a correr el programa del cliente.
La tarea se debe ejecutar periódicamente llamando a sender.exe, este ejecutable se encarga de enviar las marcadas y transmitir el personal, mostrando una barra de progreso por cada dispositivo y generando el log correspondiente.

---
## Config.ini ##

[anviz]

devices = 2 *(cantidad de dispositivos anviz en el cliente)*

log = 1 *(1: log activado, 0: log desactivado)*

[device0] *(primer dispositivo, se debe nombrar cada uno con device desde 0 en adelante)*

ip_addr = 192.168.6.54 *(dirección ip del dispositivo)*

ip_port = 5010 *(puerto utilizado por el dispositivo, 5010 es el por defecto)*

device_id = 5 *(id asignado al dispositivo)*

[device1]

ip_addr = 192.168.6.52

ip_port = 5010

device_id = 3

[api]

uri_attendance = http://webservice.tcsa.com.ar:81/API/ATTENDANCE *(url utilizada para enviar las marcadas)*

uri_staff = http://webservice.tcsa.com.ar:81/API/STAFF *(url utilizada para enviar el personal)*

customer = 2327 *(id_customer del cliente)*

---

## Instalación en raspberry pi ##
- Crear una carpeta en /home/pi llamada anviz_attendance.
- Colocar adentro los archivos config.ini, sender.py y config.ini.
- Instalar python 3.9 con las librerías necesarias.
- Crear una carpeta llamada anviz_sync en la carpeta /lib de Python 3.9
- Copiar el archivo anviz.py a la carpeta /anviz_sync anteriormente creada
- Crear un archivo llamada script.sh en /home/pi con el siguiente contenido:
cd /home/pi/anviz_attendance && python3 sender.py
- Agregar un cron cada cierto tiempo que llame a script.sh.
Ej: 0 * * * * sudo bash /home/pi/script.sh (Se ejecuta cada una hora)
Para editar el archivo de cron se utiliza "sudo crontab -e"
