---
## Instalación de dependencias ##
pip install -r requirements.txt

---
## Generar ejecutable para windows (.exe) ##
pyinstaller driver.py --onefile

---
## Crear servicio de windows ##
Se utiliza nssm para crear el servicio. (http://nssm.cc/)

La carpeta driver debe contener driver.exe, nssm.exe, config.ini y una carpeta log.

Abrir la consola, posicionarse en la ubicación donde se encuentra nssm.exe y ejecutar el comando de instalación.

cd C:\Users\Administrador\desktop\driver\

nssm install nombre_servicio "ubicacion_del_exe"

Ejemplo:
nssm install driver "C:\Users\Administrador\desktop\driver\driver.exe"

Para iniciar el servicio se ingresa al listado de servicios de windows.