import socket
import _thread
from datetime import datetime
from configparser import ConfigParser
import logging
from logging.handlers import TimedRotatingFileHandler
import sys
import time
import win32api

def main():
    win32api.SetConsoleCtrlHandler(on_exit, True)
    src_tcp_port = config.getint('config', 'src_tcp_port')
    src_udp_port = config.getint('config', 'src_udp_port')
    log = config.getint('config', 'log')
    retrys = config.getint('config', 'retrys')
    # Bind the socket to the address given on the command line
    server_address = ('', src_tcp_port)
    sock_tcp.bind(server_address)
    print('starting up TCP on {} port {}'.format(*sock_tcp.getsockname()))
    if log == 1:
        logging.warning('starting up TCP on {} port {}'.format(*sock_tcp.getsockname()))
    sock_tcp.listen(1000)
    
    for each_section in config.sections():
        if each_section != 'config':
            id = config.get(each_section, 'id')
            ip = config.get(each_section, 'ip')
            port = config.getint(each_section, 'port')
            cameras[each_section] = {'id':id, 'ip':ip, 'port':port}

    #print(cameras)
    _thread.start_new_thread(udp_server, (src_udp_port,log))
    #sock_tcp.setblocking(False)
    nro = 9000
    while True:
        connection, client_address = sock_tcp.accept()
        try:
            while True:
                data = connection.recv(4096)
                #print('received {!r}'.format(data))
                if data:
                    nro += 1 if nro < 9999 else -999
                    _thread.start_new_thread(parse_message, (data, nro, log,retrys))
                else:
                    break
        except socket.error as err:  
            # set connection status and recreate socket  
            connected = False
            logging.warning(f'Codigo 4 - {str(err)}')
            while not connected:  
                # attempt to reconnect, otherwise sleep for 2 seconds  
                try:  
                    sock_tcp.listen(1000)
                    connected = True
                    logging.warning("re-connection successful")  
                except socket.error:  
                    time.sleep( 2 )
        except Exception as e:
            logging.warning(f'Codigo 1 - {str(e)}')
            connection.close()

def udp_server(port,log):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('',port))
    print('starting up UDP on {} port {}'.format(*sock.getsockname()))
    if log == 1:
        logging.warning('starting up UDP on {} port {}'.format(*sock.getsockname()))
    try:
        while True:
            data, address_port = sock.recvfrom(4096)
            address = address_port[0]

            if data:
                datos = data.decode('ascii', 'ignore')
                if datos.startswith('>ACK;'):
                    continue
                ini_id = datos.find('ID=')
                end_id = datos[ini_id + 3:].find(';')
                id = datos[ini_id + 3:ini_id + 3 + end_id]
                ini_num = datos.find('#')
                end_num = datos[ini_num + 1:].find(';')
                num = datos[ini_num + 1:ini_num + 1 + end_num] 
                map_ip_from_id(id, address)
                if log == 1:
                    logging.warning(f'KEEPALIVE: {datos} - ID:{id} - IP:{address} - PORT:{address_port[1]}')
                ack = f'>ACK;ID={id};#{num};*'
                checksum = calc_checksum(ack)
                ack_cs = f'{ack}{checksum}<'
                sock.sendto(str.encode(ack_cs), (address, address_port[1]))
                if log == 1:
                    logging.warning(f'ACK: {datos} - ID:{id} - IP:{address} - PORT:{address_port[1]}')
    except Exception as e:
        logging.warning(f'Codigo 2 - {str(e)}')
    finally:
        sock.close()

def udp_sender(packet, ip, port, retry, message, log):
    for i in range(retry):
        sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            timeout = config.getint('config', 'timeout')
        except: 
            logging.warning(f'Not found timeout parameter in config')
        sock_udp.settimeout(1)
        try:
            sock_udp.sendto(packet, (ip, port))
        except:
            logging.warning(f'Not possible send message to ip:{ip} - port:{port}')
        try:
            response = sock_udp.recv(1024)
            if log == 1:
                logging.warning(f'Send OK - {message}')
            break
        except:
            if log == 1:
                logging.warning(f'Timeout({i+1}) - {message}')
                time.sleep(timeout)

def parse_message(pkt,n_pkt, log, retrys):
    try:
        logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO, filename = 'log/'+datetime.now().strftime('%Y_%m_%d.log'))
        datos = pkt.decode('ascii', 'ignore')
        if datos.startswith('>ACK;'):
            return
        ini_plate = datos.find('Plate')
        if ini_plate == -1:
            logging.warning('Package without Plate field')
            return
        end_plate = datos[ini_plate + 9:].find(',') - 1
        plate = datos[ini_plate + 9:ini_plate + 9 + end_plate]
        
        ini_cam = datos.find('CamName')
        if ini_cam == -1:
            logging.warning('Package without CamName field')
            return

        end_cam = datos[ini_cam + 11:].find(',') - 1
        cam_name = datos[ini_cam + 11:ini_cam + 11 + end_cam]

        id = cameras[cam_name]['id']
        cam_ip = cameras[cam_name]['ip']
        cam_port = cameras[cam_name]['port']

        payload = f'>CRD,p{plate};ID={id};#{n_pkt};*'
        checksum = calc_checksum(payload)
        payload_cs = f'{payload}{checksum}<'
        id = config.getint(cam_name, 'id')
        message = f'IP: {cam_ip} - Port: {cam_port} - Plate: {plate} - CamName: {cam_name} - Message: {payload_cs}'
        udp_sender(str.encode(payload_cs), cam_ip, cam_port, retrys, message, log)
    except:
        logging.warning(f'Package with wrong format - {pkt}')

def map_ip_from_id(id, address):
    for name, key in cameras.items():
        if cameras[name]['id'] == id:
            cameras[name]['ip'] = address

def calc_checksum(data):
    checksum = 0
    for element in data:
        checksum = checksum ^ ord(element)
    checksum = checksum ^ 42
    cs_string = '0'+str(hex(checksum))[2:]
    return cs_string[-2:].upper()

def on_exit(sig, func=None):
    logging.warning('Service Stopped by closing cmd')
    sock_tcp.close()
    logging.shutdown()

def logSetup ():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s - %(message)s',
                                    datefmt='%d-%b-%y %H:%M:%S')
    fh = TimedRotatingFileHandler('log/log', when='midnight')
    fh.suffix = "%Y%m%d.log"
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger
    
if __name__ == '__main__':
    try:
        version = 'Driver_LPR_XVM  Ver 1_07'
        print(f'Service Started - {version}')
        print('Logging configure')
        logSetup ()
        #logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO, filename = 'log/'+datetime.now().strftime('%Y_%m_%d.log'))
        logging.warning(f'Service Started - {version}')
        config = ConfigParser()
        print(f'Reading config file')
        config.read('config.ini')
        # Create a TCP/IP socket
        print(f'Create tcp socket')
        sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Create a UDP socket
        cameras = {}
        #timeout = config.getint('config', 'timeout')
        #sock_udp.settimeout(timeout)
        print(f'Init main process')
        main()
    except Exception as e:
        logging.warning(f'Codigo 3 - {str(e)}')
        time.sleep(3)
        sys.exit(str(e))
    finally:
        sock_tcp.close()
        logging.shutdown()
        logging.warning('Service Stopped')